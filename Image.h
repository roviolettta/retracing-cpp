#ifndef MAIN_IMAGE_H
#define MAIN_IMAGE_H

#include <string>

constexpr int tileSize = 16;

struct Pixel
{
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t a;

  Pixel & operator=(const Pixel & pix)
  {
    r = pix.r;
    g = pix.g;
    b = pix.b;
    a = pix.a;

    return *this;
  }
};

/*struct Color
{
  double a, r, g, b;

  void init_color(double a1, double r1, double g1, double b1)
  {
    a = a1;
    r = r1;
    g = g1;
    b = b1;
  }

  Color & operator=(const Color & color)
  {
    a = color.a;
    r = color.r;
    g = color.g;
    b = color.b;

    return *this;
  }
};*/

constexpr Pixel backgroundColor{0, 0, 0, 0};

struct Image
{
  explicit Image(const std::string &a_path);
  Image(int a_width, int a_height, int a_channels);

  int Save(const std::string &a_path);

  int Width()    const { return width; }
  int Height()   const { return height; }
  int Channels() const { return channels; }
  size_t Size()  const { return size; }
  Pixel* Data()        { return  data; }

  Pixel GetPixel(int x, int y) { return data[width * y + x];}
  void  PutPixel(int x, int y, const Pixel &pix) { data[width* y + x] = pix; }

  ~Image();

private:
  int width = -1;
  int height = -1;
  int channels = 3;
  size_t size = 0;
  Pixel *data = nullptr;
  bool self_allocated = false;
};

Pixel mix(Pixel &refract_pix, Pixel &reflect_pix);


#endif //MAIN_IMAGE_H
