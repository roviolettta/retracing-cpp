#ifndef MAIN_RAYTRACING_H
#define MAIN_RAYTRACING_H

#include <iostream>
#include <math.h>
#include "Image.h"

using namespace std;

#define EPSILON 0.0000001
#define Pi 3.14159
#define R 26
#define N 45
#define M 2*(N-1)*(N-1)

#define CROSS(dest, v1, v2)           \
  dest.x = v1.y * v2.z - v1.z * v2.y; \
  dest.y = v1.z * v2.x - v1.x * v2.z; \
  dest.z = v1.x * v2.y - v1.y * v2.x;

#define DOT(v1, v2) (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z)

#define SUB(dest, v1, v2) \
  dest.x = v1.x - v2.x;   \
  dest.y = v1.y - v2.y;   \
  dest.z = v1.z - v2.z;

#define LEN(v1, v2) (sqrt(pow(v2.x - v1.x, 2) + pow(v2.y - v1.y, 2) + pow(v2.z - v1.z, 2)))

#define NORM(dir, dist, v1, v2) \
  dir.x = (v2.x - v1.x) / dist; \
  dir.y = (v2.y - v1.y) / dist; \
  dir.z = (v2.z - v1.z) / dist;

constexpr int WIDTH = 512, HEIGHT = 512;

enum Environment
{
  Air,
  Glass
};

struct Apex
{
  double x;
  double y;
  double z;

  void init_apex(double x1, double y1, double z1)
  {
    x = x1;
    y = y1;
    z = z1;
  }

  Apex &operator=(const Apex &apex)
  {
    x = apex.x;
    y = apex.y;
    z = apex.z;

    return *this;
  }

  void scale(int k)
  {
    x *= k;
    y *= k;
    z *= k;
  }

  void print()
  {
    std::cout << "x"
              << "=" << x << "   y"
              << "=" << y << "   z"
              << "=" << z << std::endl;
  }
};

struct Material
{
  Pixel color;
  int reflection;
  int refraction;

  void init_material(Pixel &pix, const int &i, const int &j)
  {
    color = pix;
    reflection = i; //отражение
    refraction = j; //преломление
  }

  Material &operator=(const Material &material)
  {
    color = material.color;
    reflection = material.reflection;
    refraction = material.refraction;

    return *this;
  }
};

struct Triangle
{
  Apex ver0;
  Apex ver1;
  Apex ver2;
  Material material;

  void init_triangle(Apex &A0, Apex &A1, Apex &A2, Material &Mat)
  {
    ver0 = A0;
    ver1 = A1;
    ver2 = A2;
    material = Mat;
  }
};

/*struct Sphere
{
  Apex center;
  double radius;
  Pixel color;

  Sphere(const Apex &c, const double &r, const Pixel &pix) : center(c), radius(r), color(pix) {}
  bool intersect_sphere(Apex &orig, Apex &dir, double sphere_dist)
  {
    Apex L;

    SUB(L, center, orig);
    double tca = DOT(L, dir);
    double d2 = DOT(L, L) - tca * tca;
    if (d2 > radius * radius)
      return false;
    double thc = sqrt(radius * radius - d2);
    sphere_dist = tca - thc;
    double t = tca + thc;
    if (sphere_dist < 0)
      sphere_dist = t;
    if (sphere_dist < 0)
      return false;
    return true;
  }
};*/

struct Light
{
  Apex position;
  double intensity;

  void init_light(Apex &A, const double &i)
  {
    position.init_apex(A.x, A.y, A.z);
    intensity = i;
  }
};

Pixel RayTracing(Triangle T[R], Triangle Mesh[M], Triangle Box[12], Apex &orig, Apex &dir, int depth, Environment env);

#endif