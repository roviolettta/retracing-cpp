#include "Image.h"
#include "object.h"
#include "raytracing.h"
#include <vector>
#include <iostream>

using namespace std;

void init_net(double x1, double x2, double z1, double z2, double y0, Triangle Mesh[M])
{
	double x[N], z[N], y[N][N];
	int T1 = 6, T2 = 6;
	int h1 = 10;
	int h2 = 10;

	Apex Net[N][N];

	Pixel color;
	color.a = 255;
	color.b = 255;
	color.r = 0;
	color.g = 0;

	Material material;
	material.init_material(color, 0, 0);

	int dx = 0, dz = 0, u;
	for (int i = 0; i < N && dx <= (x2 - x1) && dz >= (z2 - z1); i++, dx += (x2 - x1) / N, dz += (z2 - z1) / N)
	{
		x[i] = x1 + dx;
		z[i] = z1 + dz;
	}

	for (int j = 0; j < N; j++)
	{
		for (int i = 0; i < N; i++)
		{
			y[j][i] = cos((j*Pi)/T1) * h1 + y0;
		}
	}

	for (int j = 0; j < N; j++)
	{
		for (int i = 0; i < N; i++)
		{
			y[i][j] += sin((j*Pi)/T2) * h2 ;

			Net[i][j].init_apex(x[i], y[i][j], z[j]);
		}
	}

	int k = 0;
	for (int j = 0; j < N-1; j++)
	{
		for (int i = 0; i < N-1; i++)
		{
			Mesh[k].init_triangle(Net[i][j], Net[i+1][j+1], Net[i][j+1], material);
			Mesh[k+1].init_triangle(Net[i][j], Net[i+1][j+1], Net[i+1][j], material);
			k+=2;
		}
	}

	/*for (size_t i = 0; i < M; i++)
	{
		cout<<endl<<endl<<i<<endl;
		Mesh[i].ver0.print();
		Mesh[i].ver0.print();
		Mesh[i].ver0.print();
	}*/
}

int main(int argc, char **argv)
{
	Image picture("313_romanova_v5v6.png");
	Apex A[12];

	Apex center;
	center.init_apex(0., 0., 0.);

	Pixel S_color;
	S_color.a = 255;
	S_color.b = 255;
	S_color.r = 255;
	S_color.g = 255;

	A[0].init_apex(0., 1., 0.);
	A[1].init_apex(-0.52573, 0.44721, 0.72361);
	A[2].init_apex(-0.85065, 0.44721, -0.27639);
	A[3].init_apex(0., 0.44721, -0.89443);
	A[4].init_apex(0.85065, 0.44721, -0.27639);
	A[5].init_apex(0.52573, 0.44721, 0.72361);
	A[6].init_apex(0.52573, -0.44721, -0.72361);
	A[7].init_apex(0.85065, -0.44721, 0.27639);
	A[8].init_apex(0., -0.44721, 0.89443);
	A[9].init_apex(-0.85065, -0.44721, 0.27639);
	A[10].init_apex(-0.52573, -0.44721, -0.72361);
	A[11].init_apex(0., -1., 0.);

	center.scale(1000);

	for (int i = 0; i < 12; i++)
	{
		A[i].scale(500);
	}

	center.y += 50;
	center.z -= 3500;
	for (int i = 0; i < 12; i++)
	{
		A[i].y += 50;
		A[i].z -= 3500;

		//A[i].print();
	}

	//std::cout<<"x"<<"="<<center.x<<"   y"<<"="<<center.y<<"   z"<<"="<<center.z<<std::endl;

	/*double radius = (3 + sqrt(5)) * LEN(A[0], A[1]) / (4 * sqrt(3)) - 350;
	Sphere sphere(center, radius, S_color);*/

	Triangle T[R];

	Pixel color;
	color.a = 255;
	color.b = 120;
	color.r = 100;
	color.g = 90;

	Pixel color1;
	color1.a = 255;
	color1.b = 255;
	color1.r = 130;
	color1.g = 130;

	Pixel color2;
	color2.a = 255;
	color2.b = 100;
	color2.r = 30;
	color2.g = 30;

	Material material1, material2, material3;
	material1.init_material(color, 1, 1);
	material2.init_material(color1, 0, 0);
	material3.init_material(color2, 0, 0);

	T[0].init_triangle(A[0], A[1], A[2], material1);
	T[1].init_triangle(A[0], A[2], A[3], material1);
	T[2].init_triangle(A[0], A[3], A[4], material1);
	T[3].init_triangle(A[0], A[4], A[5], material1);
	T[4].init_triangle(A[0], A[5], A[1], material1);

	T[5].init_triangle(A[2], A[9], A[1], material1);
	T[6].init_triangle(A[1], A[8], A[5], material1);
	T[7].init_triangle(A[5], A[7], A[4], material1);
	T[8].init_triangle(A[4], A[6], A[3], material1);
	T[9].init_triangle(A[3], A[10], A[2], material1);

	T[10].init_triangle(A[10], A[2], A[9], material1);
	T[11].init_triangle(A[9], A[1], A[8], material1);
	T[12].init_triangle(A[8], A[5], A[7], material1);
	T[13].init_triangle(A[7], A[4], A[6], material1);
	T[14].init_triangle(A[6], A[3], A[10], material1);

	T[15].init_triangle(A[11], A[6], A[7], material1);
	T[16].init_triangle(A[11], A[7], A[8], material1);
	T[17].init_triangle(A[11], A[8], A[9], material1);
	T[18].init_triangle(A[11], A[9], A[10], material1);
	T[19].init_triangle(A[11], A[10], A[6], material1);

	Apex B[4];
	B[0].init_apex(-7500., -600., 1200.);
	B[1].init_apex(7500., -600., 1200.);
	B[2].init_apex(-7500., -600., -13000.);
	B[3].init_apex(7500., -600., -13000.);

	T[20].init_triangle(B[0], B[1], B[2], material2);
	T[21].init_triangle(B[1], B[2], B[3], material2);

	Apex C[5];
	C[0].init_apex(0., -450, -3500.);
	C[1].init_apex(-400., -600., -3900.);
	C[2].init_apex(-400., -600., -3100.);
	C[3].init_apex(400., -600., -3900.);
	C[4].init_apex(400., -600., -3100.);

	T[22].init_triangle(C[0], C[1], C[3], material3);
	T[23].init_triangle(C[0], C[4], C[3], material3);
	T[24].init_triangle(C[0], C[4], C[2], material3);
	T[25].init_triangle(C[0], C[1], C[2], material3);

	double x1 = -180., x2 = 180., 
				 y1 = -80.,  y2 = -20., 
				 z1 = -3320., z2 = -3680.;

	Triangle Mesh[M];
	init_net(x1, x2, z1, z2, -50., Mesh);

	Triangle Box[12];
	Apex F[8];

	F[0].init_apex(x1, y1, z1);
	F[1].init_apex(x1, y2, z1);
	F[2].init_apex(x1, y1, z2);
	F[3].init_apex(x1, y2, z2);

	F[4].init_apex(x2, y1, z1);
	F[5].init_apex(x2, y2, z1);
	F[6].init_apex(x2, y1, z2);
	F[7].init_apex(x2, y2, z2);

	Box[0].init_triangle(F[0], F[1], F[5], material1);
	Box[1].init_triangle(F[0], F[5], F[4], material1);
	Box[2].init_triangle(F[0], F[2], F[6], material1);
	Box[3].init_triangle(F[0], F[6], F[4], material1);

	Box[4].init_triangle(F[0], F[1], F[3], material1);
	Box[5].init_triangle(F[0], F[3], F[2], material1);
	Box[6].init_triangle(F[1], F[3], F[7], material1);
	Box[7].init_triangle(F[1], F[7], F[5], material1);

	Box[8].init_triangle(F[2], F[3], F[7], material1);
	Box[9].init_triangle(F[2], F[7], F[6], material1);
	Box[10].init_triangle(F[4], F[5], F[7], material1);
	Box[11].init_triangle(F[4], F[7], F[6], material1);

	/*for(int i = 0; i<12; i++){
		cout<<endl<<i<<endl;
		Box[i].ver0.print();
		Box[i].ver1.print();
		Box[i].ver2.print();
	}*/

	Pixel pix;
	pix.a = 255;
	pix.b = 155;
	pix.r = 150;
	pix.g = 150;

	for (size_t j = 0; j < HEIGHT; j++)
	{
		for (size_t i = 0; i < WIDTH; i++)
		{
			picture.PutPixel(i, j, pix);
		}
	}

	Apex orig, scr;
	orig.init_apex(0., 0., 0.); //координаты камеры

	for (size_t j = 0; j < HEIGHT; j++)
	{
		for (size_t i = 0; i < WIDTH; i++)
		{
			scr.init_apex(i + 0.5 - (WIDTH - 1) / 2, j + 0.5 - (HEIGHT - 1) / 2, -1200.);

			Apex dir;
			double len = LEN(scr, orig);
			NORM(dir, len, orig, scr);

			pix = RayTracing(T, Mesh, Box, orig, dir, 5, Air);
			picture.PutPixel(i, WIDTH - j - 1, pix);
		}
	}

	picture.Save("313_romanova_v5v6.png");

	return 0;
}
