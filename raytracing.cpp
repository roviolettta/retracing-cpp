#include <math.h>
#include "Image.h"
#include "raytracing.h"
#include <iostream>

using namespace std;

bool intersect_triangle(Apex &orig, Apex &dir, Triangle &triang, double *t)
{
  double u, v;
  Apex edge1, edge2, tvec, pvec, qvec;
  double det, inv_det;

  SUB(edge1, triang.ver1, triang.ver0);
  SUB(edge2, triang.ver2, triang.ver0);

  CROSS(pvec, dir, edge2);

  det = DOT(edge1, pvec);

  if (det > -EPSILON && det < EPSILON)
  {
    return false;
  }

  inv_det = 1.0 / det;

  SUB(tvec, orig, triang.ver0);

  u = DOT(tvec, pvec) * inv_det;
  if (u < 0.0 || u > 1.0)
  {
    return false;
  }

  CROSS(qvec, tvec, edge1);

  v = DOT(dir, qvec) * inv_det;
  if (v < 0.0 || u + v > 1.0)
  {
    return false;
  }

  *t = DOT(edge2, qvec) * inv_det;
  return true;
}

Pixel illumination(Triangle &T, Triangle Mesh[M], Apex &orig, Apex &dir, Light &light, double current)
{
  Pixel diff_pix;
  diff_pix.a = 255;
  diff_pix.b = 150;
  diff_pix.r = 150;
  diff_pix.g = 140;

  Pixel pix = T.material.color;
  Apex normal;

  normal.x = (T.ver1.y - T.ver0.y) * (T.ver2.z - T.ver0.z) - (T.ver1.z - T.ver0.z) * (T.ver2.y - T.ver0.y);
  normal.y = (T.ver1.z - T.ver0.z) * (T.ver2.x - T.ver0.x) - (T.ver1.x - T.ver0.x) * (T.ver2.z - T.ver0.z);
  normal.z = (T.ver1.x - T.ver0.x) * (T.ver2.y - T.ver0.y) - (T.ver1.y - T.ver0.y) * (T.ver2.x - T.ver0.x);

  double len_normal = LEN(orig, normal);
  NORM(normal, len_normal, orig, normal);

  double diffuse = 0., specular = 0., ambient = .4;

  Apex point, view, refl_dir;
  point.init_apex(dir.x * current + orig.x, dir.y * current + orig.y, dir.z * current + orig.z);

  view.x = -dir.x;
  view.y = -dir.y;
  view.z = -dir.z;
  if (DOT(normal, view) < 0.)
  {
    normal.x = -normal.x;
    normal.y = -normal.y;
    normal.z = -normal.z;
  }
  Apex dir_light;

  double dist_to_light = LEN(point, light.position);
  NORM(dir_light, dist_to_light, point, light.position);

  double *r, d;
  bool flag = true;
  r = &d;

  Apex ray;
  ray.x = -dir_light.x;
  ray.y = -dir_light.y;
  ray.z = -dir_light.z;

  for (int iii = 0; iii < M; iii++)
  {
    if (intersect_triangle(light.position, ray, Mesh[iii], r))
    {
      if (dist_to_light - d > EPSILON && d > EPSILON)
      {
        flag = false;
        break;
      }
    }
  }

  if (flag)
  {
    double koef = abs(DOT(normal, dir_light));
    diffuse += koef * light.intensity;

    double spec = 0., cos_theta;

    dist_to_light = LEN(point, light.position);
    NORM(dir_light, dist_to_light, point, light.position);

    refl_dir.x = normal.x * 2. * DOT(normal, dir_light) - dir_light.x;
    refl_dir.y = normal.y * 2. * DOT(normal, dir_light) - dir_light.y;
    refl_dir.z = normal.z * 2. * DOT(normal, dir_light) - dir_light.z;

    cos_theta = DOT(view, refl_dir);
    if (cos_theta >= 0.)
      spec = pow(cos_theta, 1024);

    specular += spec * light.intensity;
  }

  diff_pix.b = min((diffuse + ambient + specular) * T.material.color.b, 255.);
  diff_pix.r = min((diffuse + ambient + specular) * T.material.color.r, 255.);
  diff_pix.g = min((diffuse + ambient + specular) * T.material.color.g, 255.);

  return diff_pix;
}

Pixel RayTracing(Triangle T[R], Triangle Mesh[M], Triangle Box[12], Apex &orig, Apex &dir, int depth, Environment env)
{
  int k = 0;
  depth--;
  Apex light_coords[3];
  Light light[3];

  light_coords[0].init_apex(-100., 450., -2500.);
  light[0].init_light(light_coords[0], 1.2);

  light_coords[1].init_apex(900., 300., -4400.);
  light[1].init_light(light_coords[1], 1.2);

  light_coords[2].init_apex(0., 235., -1000.);
  light[2].init_light(light_coords[2], 2.);

  Pixel pix;

  Pixel diff_pix; //цвет фона
  diff_pix.a = 255;
  diff_pix.b = 150;
  diff_pix.r = 150;
  diff_pix.g = 140;

  /*double sphere_dist;
  if(sphere.intersect_sphere(orig, dir, sphere_dist)){
      if(env == Glass)
        return sphere.color;
  }*/

  double dist, current = 0;
  double *t;

  t = &dist;
  int ii;

  if (env == Glass)
  {
    for (int i = 0; i < 12; i++)
    {
      if (intersect_triangle(orig, dir, Box[i], t))
      {
        if ((current == 0 || current >= dist) && dist > EPSILON)
        {
          current = dist;
        }
      }
    }

    if (current != 0)
    {
      current = 0;

      for (int i = 0; i < M; i++)
      {
        if (intersect_triangle(orig, dir, Mesh[i], t))
        {
          if ((current == 0 || current >= dist) && dist > EPSILON)
          {
            current = dist;
            ii = i;
          }
        }
      }

      if (current != 0)
        return illumination(Mesh[ii], Mesh, orig, dir, light[2], current);
    }
  }

  current = 0;
  for (int i = 0; i < R; i++)
  {
    if (intersect_triangle(orig, dir, T[i], t))
    {

      if ((current == 0 || current >= dist) && dist > EPSILON)
      {
        current = dist;
        ii = i;
      }
    }
    //если current осталось ==0, то пересечения нет
  }

  if (current != 0)
  {
    pix = T[ii].material.color;
    Apex normal;

    normal.x = (T[ii].ver1.y - T[ii].ver0.y) * (T[ii].ver2.z - T[ii].ver0.z) - (T[ii].ver1.z - T[ii].ver0.z) * (T[ii].ver2.y - T[ii].ver0.y);
    normal.y = (T[ii].ver1.z - T[ii].ver0.z) * (T[ii].ver2.x - T[ii].ver0.x) - (T[ii].ver1.x - T[ii].ver0.x) * (T[ii].ver2.z - T[ii].ver0.z);
    normal.z = (T[ii].ver1.x - T[ii].ver0.x) * (T[ii].ver2.y - T[ii].ver0.y) - (T[ii].ver1.y - T[ii].ver0.y) * (T[ii].ver2.x - T[ii].ver0.x);

    double len_normal = LEN(orig, normal);
    NORM(normal, len_normal, orig, normal);

    double diffuse = 0., specular = 0., ambient = .1;

    Apex point, view, refl_dir;
    point.init_apex(dir.x * current + orig.x, dir.y * current + orig.y, dir.z * current + orig.z);

    view.x = -dir.x;
    view.y = -dir.y;
    view.z = -dir.z;
    if (DOT(normal, view) < 0.)
    {
      normal.x = -normal.x;
      normal.y = -normal.y;
      normal.z = -normal.z;
    }

    if (depth != 0 && T[ii].material.reflection == 1 && T[ii].material.refraction == 1)
    {
      Pixel refl_pix = diff_pix; // отраженный пиксель
      Pixel refr_pix = diff_pix; //преломленный пиксель

      //********Отражение********
      refl_dir.x = normal.x * 2. * DOT(normal, view) - view.x;
      refl_dir.y = normal.y * 2. * DOT(normal, view) - view.y;
      refl_dir.z = normal.z * 2. * DOT(normal, view) - view.z;

      refl_pix = RayTracing(T, Mesh, Box, point, refl_dir, depth, env);

      //*******Преломление********
      double refract_koef;
      if (env == Air)
      {
        refract_koef = 2 / 3;
        env = Glass;
      }
      else
      {
        refract_koef = 3 / 2;
        env = Air;
      }

      double k = 1 - pow(refract_koef, 2) * (1 - pow(DOT(normal, view), 2));
      if (k >= 0)
      {
        Apex refr_dir;
        refr_dir.x = dir.x * refract_koef + normal.x * (refract_koef * DOT(normal, view) - sqrt(k));
        refr_dir.y = dir.y * refract_koef + normal.y * (refract_koef * DOT(normal, view) - sqrt(k));
        refr_dir.z = dir.z * refract_koef + normal.z * (refract_koef * DOT(normal, view) - sqrt(k));

        //if (depth != 0)
        refr_pix = RayTracing(T, Mesh, Box, point, refr_dir, depth, env);
      }

      else //полное внутреннее отражение
        refr_pix = refl_pix;   

      diff_pix = mix(refr_pix, refl_pix);
    }

    for (int l = 0; l < 2; l++)
    {
      Apex dir_light;

      double dist_to_light = LEN(point, light_coords[l]);
      NORM(dir_light, dist_to_light, point, light_coords[l]);

      double *r, d;
      bool flag = true;
      r = &d;

      Apex ray;
      ray.x = -dir_light.x;
      ray.y = -dir_light.y;
      ray.z = -dir_light.z;

      for (int iii = 0; iii < R; iii++)
      {
        if (intersect_triangle(light_coords[l], ray, T[iii], r))
        {
          if (dist_to_light - d > EPSILON && d > EPSILON)
          {
            flag = false;
            break;
          }
        }
      }

      if (flag)
      {
        double koef = abs(DOT(normal, dir_light));
        diffuse += koef * light[l].intensity;

        double spec = 0., cos_theta;

        dist_to_light = LEN(point, light_coords[l]);
        NORM(dir_light, dist_to_light, point, light_coords[l]);

        refl_dir.x = normal.x * 2. * DOT(normal, dir_light) - dir_light.x;
        refl_dir.y = normal.y * 2. * DOT(normal, dir_light) - dir_light.y;
        refl_dir.z = normal.z * 2. * DOT(normal, dir_light) - dir_light.z;

        cos_theta = DOT(view, refl_dir);
        if (cos_theta >= 0.)
          spec = pow(cos_theta, 1024);

        specular += spec * light[l].intensity;
      }

      if (T[ii].material.reflection == 0 && T[ii].material.refraction == 0)
      {
        diff_pix.b = min((diffuse + ambient + specular) * T[ii].material.color.b, 255.);
        diff_pix.r = min((diffuse + ambient + specular) * T[ii].material.color.r, 255.);
        diff_pix.g = min((diffuse + ambient + specular) * T[ii].material.color.g, 255.);
      }

      else if (depth == 4)
      {
        diff_pix.b = min((1 + specular) * diff_pix.b, 255.);
        diff_pix.r = min((1 + specular) * diff_pix.r, 255.);
        diff_pix.g = min((1 + specular) * diff_pix.g, 255.);
      }
    }
  }

  return diff_pix;
}